---
lang: zh-tw
---
# ETToday新聞雲專案心得
:::info
:bulb: **目的**: 新聞雲程式開發心得
:::
## 目錄

[TOC]


## Tip
1.  專案GUI(Storyboard &  Xib)要留意 
 -  runtime attribution，兩者很難被search。以前會約定避免使用
 -  @IBDesignable 盡量只設定UIView的就好而且會吃Xcode版本(有些版本會閃退)
-  custom view by xib 盡量避免constraint&animate會吃Xcode版本(有些版本會閃退)
2. Print
- log有分階，避免直接使用print，使用ETLogging


## 專案程式架構個人心得
  1. AppDelegate 初始化＆ MainViewController
        - first viewcontroller is MainViewController, variable name is drawer
        - drawer 將會以單例給予其他類存取
        - drawer 初始不在 didFinishLaunchingWithOptions 而是在 supportedInterfaceOrientationsFor
        -  drawer 會在內部 AppDelegate.main?.drawer = self; 指派給自己
        (這只能用在root vc)
 2. Presenter & View 交互流程
        - presneter 初始方法必須有實作協定物件as參數
 3.  逆向查找方式
    - 面對既有專案，個人比較頃向從ImageName或label比較特殊字串開始尋找
4. storyBoard 在多國語系有使用MMLocalization 在runtime時將label text 方法置換達成多國語系的功能，其label的text只是一個key

   ```flow
st=>start: icon name or lable text 
op=>operation: find imageView of label in stroyboard or xib
op1=>operation: find the viewcontroller
e=>end: find the presnter

st->op->op1->e

```
